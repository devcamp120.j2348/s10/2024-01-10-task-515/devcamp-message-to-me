import { Component } from "react";
import background from "../../../assets/images/background.jpg";

class Image extends Component {
    render() {
        return (
            <div className='row'>
                <div className='col-12 text-center'>
                    <img alt="title" src={background}/>
                </div>
            </div>
        )
    }
}

export default Image;