import { Component } from "react";
import Title from "./header-title/title.component";
import Image from "./header-image/image.component";

class Header extends Component {
    render() {
        return (
            <>
                <Title />
                <Image />
            </>
        )
    }
}

export default Header;