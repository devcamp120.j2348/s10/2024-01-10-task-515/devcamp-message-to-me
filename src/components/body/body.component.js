import { Component } from "react";
import Input from "./body-input/input.component";
import Output from "./body-output/output.component";

class Body extends Component {
    render() {
        return (
            <>
                <Input />
                <Output />
            </>
        )
    }
}

export default Body;