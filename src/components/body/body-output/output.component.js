import { Component } from "react";
import likeImage from "../../../assets/images/like.png";

class Output extends Component {
    render() {
        return (
            <>
                <div className='row mt-3'>
                    <div className='col-12 text-center'>
                        <img alt="title" src={likeImage} width={100}/>
                    </div>
                </div>
                <div className='row mt-3'>
                    <div className='col-12 text-center'>
                        <p>Thông điệp ở đây</p>
                    </div>
                </div>
            </>
        )
    }
}

export default Output;