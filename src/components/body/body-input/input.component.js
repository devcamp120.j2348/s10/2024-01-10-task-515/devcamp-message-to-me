import { Component } from "react";

class Input extends Component {

    onInputChangeHandler(event) {
        console.log("Sự kiện ô input thay đổi giá trị");
        var inputValue = event.target.value;
        console.log(inputValue);
    }

    onButtonClickHandler() {
        console.log("Sự kiện bấm chuột gửi thông điệp");
    }

    render() {
        return (
            <>
                <div className='row mt-3'>
                    <div className='col-12'>
                        <label>Message cho bạn 12 tháng tới:</label>
                        <input className='form-control' onChange={this.onInputChangeHandler}/>
                    </div>
                </div>
                <div className='row mt-3'>
                    <div className='col-12 text-center'>
                        <button className='btn btn-success' onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                    </div>
                </div>
            </>
        )
    }
}

export default Input;